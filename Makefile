OBJECTS		 = main.o
OUTPUT		 = sooparse
DEFS		 = 
OPTIMIZE	 = -O2
LIBS		 = 
LIBDIRS		 = 
INCLUDES	 = -Iinclude -I.
CFLAGS		 = -g -std=c89 -pedantic -Wall -Werror $(INCLUDES) $(OPTIMIZE)
LDFLAGS		 = $(LIBDIRS) $(LIBS)
CC		 = gcc
RM		 = rm

all: $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
run:
	./$(OUTPUT)
install:
	install -s $(OUTPUT) /usr/bin/$(OUTPUT)
release: clean
