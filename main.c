#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

FILE* nullsink;

void copy_char(char ch, FILE* ofp)
{
	fprintf(ofp, "%c", ch);
}

int jump_comma(FILE* ifp)
{
	if(fgetc(ifp) != ',') {
		fseek(ifp, -1, SEEK_CUR);
		return 0;
	}
	return 1;
}

void copy_symbol_name(FILE* ifp, FILE* ofp)
{
	char ch;
	int done = 0;
	int parens = 0;
	int hit_dash = 0;
	int bracks = 0;
	for(ch = fgetc(ifp); !done && !feof(ifp); ch = fgetc(ifp)) {
		switch(ch) {
			case '>':
				if(hit_dash == 1) {
					hit_dash = 0;
					copy_char('-', ofp);
					copy_char(ch, ofp);
					break;
				}
			case '<': case '{': case '}':
			case '/': case '+': case ' ':
			case ',': case ';':
				if(!parens && !bracks) {
					fseek(ifp, -1 - hit_dash, SEEK_CUR);
					done = 1;
				}else{
					copy_char(ch, ofp);
				}
				break;
			case '[':
				bracks++;
				copy_char(ch, ofp);
				break;
			case ']':
				copy_char(ch, ofp);
				bracks--;
				break;
			case '(':
				parens++;
				copy_char(ch, ofp);
				break;
			case ')':
				if(parens == 0) {
					fseek(ifp, -1 - hit_dash, SEEK_CUR);
					done = 1;
					break;
				}
				copy_char(ch, ofp);
				parens--;
				break;
			case '-':
				hit_dash = 1;
				break;
			default:
				if(hit_dash == 1) {
					fseek(ifp, -2, SEEK_CUR);
					done = 1;
					break;
				}
				copy_char(ch, ofp);
				break;
		}
	}
	fseek(ifp, -1, SEEK_CUR);
}

void copy_arguments(FILE* ifp, FILE* ofp)
{
	char ch;
	int cont = 1;
	for(ch = fgetc(ifp); cont && !feof(ifp); ch = fgetc(ifp)) {
		switch(ch) {
			case '(':
				cont++;
				break;
			case ')':
				cont--;
				break;
			default:
				break;
		}
		if(cont)
			copy_char(ch, ofp);
	}
	fseek(ifp, -1, SEEK_CUR);
}

void copy_argument(FILE* ifp, FILE* ofp)
{
	char ch;
	int cont = 1;
	for(ch = fgetc(ifp); cont && !feof(ifp); ch = fgetc(ifp)) {
		switch(ch) {
			case '(':
				cont++;
				break;
			case ')':
				cont--;
				break;
			case ',':
				cont = 0;
				break;
			default:
				break;
		}
		if(cont)
			copy_char(ch, ofp);
	}
	fseek(ifp, -1, SEEK_CUR);
}

int is_whitespace(char ch)
{
	switch(ch) {
		case ' ':
		case '\t':
			return 1;
		default:
			return 0;
	}
}

void skip_whitespace(FILE* ifp)
{
	char ch;
	int done = 0;
	for(ch = fgetc(ifp); !done && !feof(ifp); ch = fgetc(ifp)) {
		if(!is_whitespace(ch)) {
			fseek(ifp, -1, SEEK_CUR);
			done = 1;
		}
	}
	fseek(ifp, -1, SEEK_CUR);
}

int read_cmd(FILE* ifp)
{
	int cmd = -1, cmdidx = 0;
	char command[17];
	char ch;
	int done = 0;
	for(ch = fgetc(ifp); !done && !feof(ifp) && (cmdidx < 16); ch = fgetc(ifp)) {
		if(is_whitespace(ch) || (ch == '(')) {
			fseek(ifp, -1, SEEK_CUR);
			done = 1;
			continue;
		}
		command[cmdidx++] = ch;
	}
	fseek(ifp, -1, SEEK_CUR);
	for(; cmdidx <= 16; cmdidx++)
		command[cmdidx] = 0;
	skip_whitespace(ifp);
	if(fgetc(ifp) != '(') {
		fprintf(stderr, "Error: Command requires '(' after name, command %s\n", command);
		return -1;
	}
	if(strncmp(command, "alloc", 16) == 0)
		cmd = 0;
	else if(strncmp(command, "destroy", 16) == 0)
		cmd = 1;
	else if(strncmp(command, "", 16) == 0)
		cmd = 2;
	else if(strncmp(command, "call", 16) == 0)
		cmd = 2;
	else if(strncmp(command, "class", 16) == 0)
		cmd = 3;
	else if(strncmp(command, "endclass", 16) == 0)
		cmd = 4;
	else if(strncmp(command, "declare", 16) == 0)
		cmd = 5;
	else if(strncmp(command, "func", 16) == 0)
		cmd = 6;
	else if(strncmp(command, "register", 16) == 0)
		cmd = 7;
	else if(strncmp(command, "fast", 16) == 0)
		cmd = 8;
	else if(strncmp(command, "fastcall", 16) == 0)
		cmd = 8;
	else if(strncmp(command, "fastdeclare", 16) == 0)
		cmd = 9;
	else if(strncmp(command, "fastdecl", 16) == 0)
		cmd = 9;
	else if(strncmp(command, "funcname", 16) == 0)
		cmd = 10;
	else {
		fprintf(stderr, "Error: Unknown command %s\n", command);
		cmd = -1;
	}
	return cmd;
}

int parse_soo(char* infile, char* outfile)
{
	int i;
	int ret = 0;
	int tmppos;
	char ch;
	char tmpname[8];
	int in_string = 0, in_comment = 0, in_class = 0;
	/* Multi-pass shit */
	int did_parse = 0;
	static int passnum = 0;
	/* Comment type/detection */
	int in_comment_tmp = 0;
	FILE* ifp;
	FILE* ofp;
	(void)in_class;
	sprintf(tmpname, "tmp.%03d", passnum);
	ifp = fopen(infile, "rb");
	if(ifp == NULL) {
		fprintf(stderr, "Can't open %s\n", infile);
		return 1;
	}
	ofp = fopen(tmpname, "wb+");
	if(ofp == NULL) {
		fprintf(stderr, "Can't open %s\n", tmpname);
		return 1;
	}
	for(ch = fgetc(ifp); !feof(ifp); ch = fgetc(ifp)) {
		switch(ch) {
			case '@':
				if(in_string || in_comment) {
					copy_char(ch, ofp);
					break;
				}
				switch(read_cmd(ifp)) {
					case -1: /* Error */
						did_parse = 0;
						ret = 1;
						goto end_parse;
					case 0: /* @alloc */
						fprintf(ofp, "objAlloc(");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 1: /* @destroy */
						fprintf(ofp, "objDestroy(");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 2: /* @call */
						fprintf(ofp, "objDo(");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 3: /* @class */
						fprintf(ofp, "typedef struct _");
						tmppos = ftell(ifp);
						copy_symbol_name(ifp, ofp);
						fseek(ifp, tmppos, SEEK_SET);
						fprintf(ofp, " ");
						copy_symbol_name(ifp, ofp);
						fseek(ifp, tmppos, SEEK_SET);
						fprintf(ofp, ";\n");
						fprintf(ofp, "struct _");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						did_parse = 1;
						break;
					case 4: /* @endclass */
#if 0
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
#else
						copy_symbol_name(ifp, nullsink);
						copy_arguments(ifp, nullsink);
#endif
						did_parse = 1;
						break;
					case 5: /* @declare */
						fprintf(ofp, "objDeclare(");
						copy_argument(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						if(jump_comma(ifp))
							fprintf(ofp, ",");
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 6: /* @func */
						fprintf(ofp, "objDoDeclare(");
						copy_argument(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						if(jump_comma(ifp))
							fprintf(ofp, ",");
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 7: /* @register */
						fprintf(ofp, "objDoInit(");
						copy_argument(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 8: /* @fastcall */
						fprintf(ofp, "objFastDo(");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 9: /* @fastdeclare */
						fprintf(ofp, "objFastDeclare(");
						copy_argument(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						if(jump_comma(ifp))
							fprintf(ofp, ",");
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					case 10: /* @funcname */
						fprintf(ofp, "objDoName(");
						copy_symbol_name(ifp, ofp);
						skip_whitespace(ifp);
						fprintf(ofp, ", ");
						copy_symbol_name(ifp, ofp);
						copy_arguments(ifp, ofp);
						fprintf(ofp, ")");
						did_parse = 1;
						break;
					default:
						fprintf(stderr, "Error: Unknown command.\n");
						did_parse = 0;
						ret = 1;
						goto end_parse;
				}
				break;
			case '\"':
				if(!in_comment)
					in_string ^= 1;
				copy_char(ch, ofp);
				break;
			case '\n':
				if(in_comment_tmp == 2) {
					in_comment = 0;
					in_comment_tmp = 0;
				}
				copy_char(ch, ofp);
				break;
			case '/':
				if(in_string) {
					copy_char(ch, ofp);
					break;
				}
				if(in_comment_tmp == 0) {
					if(fgetc(ifp) == '*') {
						in_comment_tmp = 4;
						in_comment = 1;
						fseek(ifp, -1, SEEK_CUR);
					}else{
						fseek(ifp, -1, SEEK_CUR);
						if(fgetc(ifp) == '/') {
							in_comment = 1;
							in_comment_tmp = 2;
							fseek(ifp, -1, SEEK_CUR);
						}else{
							fseek(ifp, -1, SEEK_CUR);
						}
					}
				}
				copy_char(ch, ofp);
				break;
			case '*':
				if(in_string) {
					copy_char(ch, ofp);
					break;
				}
				if(in_comment_tmp == 4) {
					if(fgetc(ifp) == '/') {
						in_comment_tmp = 0;
						in_comment = 0;
					}
					fseek(ifp, -1, SEEK_CUR);
				}
				copy_char(ch, ofp);
				break;
			default:
				copy_char(ch, ofp);
				break;
		}
	}
end_parse:
	/* Keep passing until we parse the whole thing */
	if(did_parse) {
		fclose(ifp);
		fclose(ofp);
		passnum++;
		return parse_soo(tmpname, outfile);
	}else{
		fclose(ifp);
		fclose(ofp);
		ifp = fopen(tmpname, "rb");
		ofp = fopen(outfile, "wb+");
		fprintf(ofp, "/* SOO support */\n#include <soo.h>\n\n");
		for(ch = fgetc(ifp); !feof(ifp); ch = fgetc(ifp)) fprintf(ofp, "%c", ch);
		fclose(ifp);
		fclose(ofp);
		for(i = 0; i <= passnum; i++) {
			sprintf(tmpname, "tmp.%03d", i);
			unlink(tmpname);
		}
		fclose(nullsink);
		return ret;
	}
}

void usage(char *app)
{
	fprintf(stderr, "Invalid arguments.\n");
}

int main(int argc, char *argv[])
{
	FILE* soofp;
	if(argc < 3) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	nullsink = fopen("/tmp/garbage.xxx", "wb+");
	soofp = fopen("soo.h", "wb");
	fprintf(soofp,
		"/*****************************************************************************\n"
		" *  SOO support                                                              *\n"
		" *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*\n"
		" *  Copyright (C)2010-2011     trap15 (Alex Marshall) <trap15@raidenii.net>  *\n"
		" *  Licensed under the MIT license.                                          *\n"
		" *****************************************************************************/\n");
	fprintf(soofp,
		"#ifndef __SOO_H__\n"
		"#define __SOO_H__\n");
	fprintf(soofp,
		"#define objDispatch(obj, call, args...)			((obj)->call((obj), args))\n"
		"#define objDo(obj, call...)				objDispatch((obj), call, NULL)\n"
		"#define objDoName(kind, call)				_soocall_##kind##_##call\n"
		"#define objFastDispatch(kind, obj, call, args...)	objDoName(kind, call)((obj), args)\n"
		"#define objFastDo(kind, obj, call...)			objFastDispatch(kind, (obj), call, NULL)\n");
	fprintf(soofp,
		"#define objDeclaration(kind, type, call, args...)	type	(*call)(struct _##kind*, args)\n"
		"#define objDeclare(type, kind, call...)			objDeclaration(kind, type, call, void*)\n"
		"#define objDoDeclaration(kind, type, call, args...)	type objDoName(kind, call)(kind* self, args)\n"
		"#define objDoDeclare(type, kind, call...)		objDoDeclaration(kind, type, call, void* none)\n"
		"#define objFastDeclare(type, kind, call...)		extern objDoDeclaration(kind, type, call, void* none)\n");
	fprintf(soofp,
		"#define objDoInit(obj, kind, call)			obj->call = objDoName(kind, call)\n"
		"#define objAlloc(kind)					(kind##_alloc())\n"
		"#define objDestroy(obj)					objDo(obj, release)\n");
	fprintf(soofp,
		"#define BOOL int\n"
		"#define FALSE 0\n"
		"#define TRUE 1\n");
	fprintf(soofp,
		"#endif\n");
	fclose(soofp);
	return (parse_soo(argv[1], argv[2]) == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
